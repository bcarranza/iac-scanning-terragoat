# 🌎🐐🌎🐐🌎🐐🌎🐐🌎🐐🌎🐐🌎🐐🌎🐐🌎🐐🌎🐐🌎🐐🌎🐐🌎🐐🌎🐐

Scan vulnerable-by-design Terraform files [courtesy of Terragoat](https://github.com/bridgecrewio/terragoat) with [Infrastructure as Code (IaC) Scanning](https://docs.gitlab.com/ee/user/application_security/iac_scanning/) in GitLab CI. :robot: 
